# Bot_REGOD New
`v1.0.0` Stable<br/><br/>

The REddit_Gatherer_Of_Data (REGOD) is a bot developed to get data from the top 100 subreddits.

    Objective:	To get the Popularity of top 10 posts in a subreddit sorted by top - 'hour' every 15 minutes.
    Library:  PRAW6
    PopularityIndex:  Upvotes+Downvotes or Number_of_Comments
    Use Case: This data will be used to figure out what time of the day is the best to post to get maximum number of views
    Deploy Time:  1 Month

The old bot (worked on some other algo) is <a href="https://github.com/IceCereal/Bot_REGOD">here</a><br/><br/>
Main Bot: <a href="https://github.com/IceCereal/Bot_REGOD-New/blob/master/regod.py">REGOD</a><br/>
Subreddit List: <a href="https://github.com/IceCereal/Bot_REGOD-New/blob/master/ListOfSubreddits">ListOfSubreddits</a><br/>
PostVars: <a href="https://github.com/IceCereal/Bot_REGOD-New/blob/master/PostVars.txt">A file of all the variables (JSON) that a post has</a><br/><br/>
